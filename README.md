# Data migration plugin

This plugin exports and imports *all* articles of a specific user via neutral format (JSON files in a ZIP archive). This way you can migrate your entire article dataset between different tt-rss instances even if they are not sharing a common database backend, i.e. MySQL to PostgreSQL.

Note that this only migrates articles. You can transfer your tt-rss settings and feed subscriptions via OPML import/export before migrating data with this plugin.

Migrating between different tt-rss versions might work but is not recommended.

Plugin is in development, I suggest testing your import first on a separate test user (or database) for the time being.

### Using the plugin

1. Use plugin installer in preferences or git checkout manually to ``plugins.local/data_migration``;
2. [Enable the plugin globally](https://tt-rss.org/wiki/GlobalConfig) by adding ``data_migration`` to ``PLUGINS`` directive;
3. Plugin-specific options should appear in ``update.php`` help output (``php ./update.php --help``);

#### Exporting data

Using docker compose:

```
docker compose exec -u app -w /var/www/html/tt-rss app php82 ./update.php --data_user test --data_export /tmp/test.zip
docker compose cp app:/tmp/test.zip .
```

On the host:

```
php ./update.php --data_user test --data_export articles-of-user-test.zip
```

#### Importing data

```
php ./update.php --data_user test --data_import articles-of-user-test.zip
```
